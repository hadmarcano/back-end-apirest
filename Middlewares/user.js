const User = require("../models/user");


// User Middlewares ...

exports.signup = async (req, res) => {
  const user = new User(req.body);
  
  
  try {
    const token = await user.generateAuthToken();
    
    await user.save();
    res.status(201).send({ user, token });
  } catch (e) {
    res.status(400).send({error: e});
  }
};

exports.signin = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findByCredentials(email, password);
    if (user === {}) {
      return res.status(400).send("Unable to connect");
    }

    const token = await user.generateAuthToken();

    res.send({ user, token });
  } catch (e) {
    res.status(400).send({ error: "Unable to login" });
  }
};

exports.read = async (req, res) => {
  res.send(req.user);
};

exports.userById = async (req, res) => {
  const _id = req.params.id;

  try {
    const user = await User.findById(_id);
    if (!user) {
      return res.status(404).send();
    }

    res.send(user);
  } catch (e) {
    res.status(500).send({error: 'User not found'});
  }
};

exports.listUsers = async (req, res) => {

    try{
        const users = await User.find();
        res.send(users);
    }catch (e){
        res.status(500).send(e);
    }

};

exports.update = async (req, res) => {
  const _id = req.params.id;
  const updates = Object.keys(req.body);
  const allowedUpdates = ["name", "email", "password", "status"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    res.status(400).send({ Error: "Invalid update" });
  }
  try {
    const user = await User.findById(_id);
    if (!user) {
      return res.status(400).send();
    }

    updates.forEach((update) => (user[update] = req.body[update]));

    await user.save();

    res.send(user);
  } catch (e) {
    res.status(500).send(e);
  }
};

exports.deleteUser = async (req, res) => {
  const _id = req.params.id;

  try {
    const user = await User.findByIdAndDelete(_id);

    if (!user) {
      return res.status(404).send();
    }

    res.send(user);
  } catch (e) {
    res.status(500).send(e);
  }
};
