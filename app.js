// Generic Import
const express = require("express");
const morgan = require("morgan");
const cors = require('cors');
const db = require('./db/mongoose');
require("dotenv").config();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

//import Routes

const userRouter = require("./routes/user");

// App-express

const app = express();

// DB Execution Connection

db();

// Middlewares

app.use(express.json());
app.use(morgan("dev"));
app.use(cors());

const swaggerOptions = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            Version: '1.0.0',
            title: 'TEST API Rest Server',
            description: 'API Documentation for use',
            contact: {
                name: 'Héctor Adolfo Díaz Marcano',
                url: 'https://www.linkedin.com/in/hector-adolfo-diaz-marcano-ab0a27aa/'
            },
            servers: ['http://localhost:3000']

        }
    },
    // APIs to documents with swagger ...
    apis: ['./routes/user.js']

};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// Router Middlewares

app.use('/api', userRouter);

// PORT

const port = process.env.PORT || 5000;

// Listen PORT

app.listen(port, () => {
  console.log(`The server is listening on port ${port}`);
});
