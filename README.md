# Test técnico Nodejs
## Cargo BackEnd - Duración 4 hrs



## API Rest con NodeJS, Express y MongoDB:


API Rest utilizando nodejs, expressjs y MongoDB:

1. Con esta API puedes realizar operaciones con datos de usuarios como crear, actualizar, eliminar y buscar uno o varios usuarios.

2. Schema de datos

```javascript
{
    id: integer,
    name: string,
    email: string,
    password: string,
    status: string,
}
```

## REQUERIMIENTOS
Necesitas tener instalado:
- NodeJS
- MongoDB

## VARIABLES DE ENTORNO
Esta api necesita de las siguientes variables de entorno:
- DATABASE=mongodb://localhost/backendrestserverbbddd
- PORT=3000
- JWT_SECRET=testapirest

## INICIALIZACION DE API
Para iniciar la API es necesario ejecutar los siguientes comandos desde la ubicación del directorio que contiene el archivo app.js y el package.json :
- npm install

#### Iniciar Mongodb
- mongod

#### Modo 'start'
- npm run start

#### Modo 'dev
- npm run dev

## DOCUMENTACION API
Esta API esta documentada con swagger y openapi, para acceder a la documentación
luego de iniciar la API acceda a la siguiente dirección desde su navegador:

- http://localhost:3000/api-docs





### ¿Tienes dudas? escríbenos a postulaciones@frenon.com

