const express = require('express');
const router = new express.Router();
const User = require('../models/user');

// Import Middlewares ...

const auth = require('../Middlewares/auth');

const {
    signup,
    signin,
    read,
    userById,
    listUsers,
    update,
    deleteUser
 } = require('../Middlewares/user');

// Routes ...

/**
 * @swagger
 *  components:
 *    securitySchemes:
 *      bearerAuth:
 *        type: http
 *        scheme: bearer
 *        bearerFormat: JWT
 */


/**
 * @swagger
 *  definitions:
 *   NewUser:
 *      type: object
 *      required:
 *         - email
 *         - password
 *      properties:
 *         name:
 *             type: string
 *         email:
 *             type: string
 *         password:
 *             type: string
 *         status:
 *             type: string
 */

/**
 * @swagger
 * /api/users:
 *  post:
 *     summary: Create a User or User Register
 *     description: Use to request an user register
 *     requestBody: 
 *       content:
 *           application/json:
 *             schema:
 *              $ref: '#definitions/NewUser'
 *     produces:
 *       - application/json
 *     responses:
 *       "200":
 *          description: Object with user registered!
 *       "400":
 *          description: A bad request response!
 */
router.post('/users', signup);

/**
 * @swagger   
 * /api/users/login: 
 *  post:
 *    summary: Login user
 *    description: Use to request signin or login user
 *    requestBody: 
 *      content:
 *        application/json:
 *          schema:
 *            properties:
 *              email:
 *                  type: string
 *                  required: true
 *                  description: email user valid
 *              password:
 *                  type: string
 *                  required: true
 *                  description: password user valid
 *    responses:
 *      "200":
 *         description: A successful response an object with the user and token
 *      "400":
 *         description: Unable to login
 */
router.post('/users/login', signin);

/**
 * @swagger
 * /api/users/me:
 *  get:
 *    security:
 *      - bearerAuth: []
 *    summary: Get user profile
 *    description: Use to request user profile, to access the profile, please authenticate with the token generated when logging in
 *    responses:
 *      "200":
 *         description: An object with the user profile
 *      "400": 
 *         description: A bad request response!
 *    
 */
router.get('/users/me', auth, read);

/**
 * @swagger
 * /api/users/{id}:
 *  get:
 *      summary: Get User by ID
 *      description: Use to request an user by ID
 *      produces:
 *         - application/json
 *      parameters:
 *         - in: path
 *           name: id
 *           description: ID of user
 *      responses:
 *        "200":
 *           description: Object with the user by Id
 *        "400":
 *           description: User not Found!
 */
router.get('/users/:id', userById);

/**
 * @swagger
 * /api/listusers:
 *  get:
 *      summary: List users
 *      description: Use this request to List all the users
 *      produces:
 *         - application/json
 *      responses:
 *         "200":
 *             description: Object with all the users
 *         "400":
 *             description: A bad request response!     
 */
router.get('/listusers', listUsers);

/**
 * @swagger
 * /api/users/{id}:
 *  patch:
 *     summary: Update User by ID
 *     description: Use this request to update an user searched by ID
 *     requestBody:
 *       content:
 *           application/json:
 *             schema:
 *              $ref: '#definitions/NewUser'
 *     produces:
 *        - application/json
 *     parameters:
 *        - in: path
 *          name: id
 *          description: ID of user
 *     responses:
 *        "200": 
 *           description: Object with the user updated!
 *        "400": 
 *           description: A bad request response!
 */
router.patch('/users/:id', update);


/**
 * @swagger
 * /api/users/{id}:
 *  delete:
 *        summary: Delete User by ID
 *        description: Use to delete an user by ID
 *        produces:
 *           - application/json
 *        parameters:
 *           - in: path
 *             name: id
 *             description: ID of user
 *        responses:
 *          "200":
 *              description: An object with the user deleted!
 *          "400":
 *              description: A bad request response!
 */
router.delete('/users/:id', deleteUser);

module.exports = router;




